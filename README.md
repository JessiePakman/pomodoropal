# PomodoroPal
PomodoroPal is a command line tool to help you use,create and manage your own pomodoro timers.

#How to use

**Install rust:**
https://www.rust-lang.org/tools/install

**If you're on windows, you will also need to install git bash:**
https://git-scm.com/download/win

**then open your terminal/git bash and clone the repo:**
`git clone https://gitlab.com/JessiePakman/pomodoropal`

**then go to where you cloned the repo:**
`cd /path/to/project`

**run it:**
`cargo run`
