use std::{
    fs::File,
    io::{self, BufRead, BufReader, Read, Write},
};

use crate::create_presets::UserPreset;
pub fn create_preset_file() -> std::io::Result<File> {
    let file = File::create("user_presets.json")?;
    Ok(file)
}

pub fn write_to_file(data: &Vec<UserPreset>) -> std::io::Result<()> {
    let mut file = create_preset_file()?;
    let data = serde_json::to_string(&data)?;
    _ = file.write_all(data.as_bytes());
    Ok(())
}

pub fn read_presets_file() -> std::io::Result<Vec<UserPreset>> {
    let file = File::open("./user_presets.json")?;
    let mut buffer = BufReader::new(file);
    let mut contents = String::new();
    _ = buffer.read_to_string(&mut contents);
    let created_presets: Vec<UserPreset> = serde_json::from_str(&contents)?;

    if contents.is_empty() {
        println!("you haven't made any presets yet!");
    } else {
        println!("Here are the presets you made: ");
        println!("{:?}", created_presets)
    }
    Ok(created_presets)
}
