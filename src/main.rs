use std::{
    io::{self, Write},
    thread,
    time::Duration,
    vec,
};

use create_presets::{TimerPreset, UserPreset};

use crate::{
    create_presets::build_user_preset,
    read_and_write_file::{create_preset_file, read_presets_file, write_to_file},
};
mod create_presets;
mod playsound;
mod read_and_write_file;
fn main() {
    let mut user_presets: Vec<UserPreset> = Vec::new();
    let preset_1 = create_presets::build_preset(
        1,
        String::from("25min preset \n work time : 25mins ,  break time : 5mins"),
        1500,
        300,
    );
    let preset_2 = create_presets::build_preset(
        2,
        String::from("30min preset \n work time : 30mins ,  break time : 10mins"),
        1800,
        600,
    );
    let preset_3 = create_presets::build_preset(
        3,
        String::from("50min preset \n work time : 50mins ,  break time : 20mins"),
        3000,
        1200,
    );
    println!(" 0 -> Show available presets");
    println!(
        " 1 -> Work for {} minutes and have {} minute break",
        preset_1.work_time / 60,
        preset_1.break_time / 60
    );
    println!(
        " 2 -> Work for {} minutes and have {} minute break",
        preset_2.work_time / 60,
        preset_2.break_time / 60
    );
    println!(
        " 3 -> Work for {} minutes and have {} minute break",
        preset_3.work_time / 60,
        preset_3.break_time / 60
    );
    println!(" 4 -> Make your own preset");
    println!(" 5 -> use your presets");
    let mut input = String::new();
    io::stdin()
        .read_line(&mut input)
        .expect("could not read input");
    let user_choice: i32 = input
        .trim()
        .parse()
        .expect("you typed {}.your input must be a number like: 1 , 2 ,3.");
    let available_presets = vec![&preset_1, &preset_2, &preset_3];

    if user_choice == 0 {
        println!("Here are the presets you can use!:");
        for available_preset in &available_presets {
            println!("{:?}", available_preset);
        }
    } else if user_choice == 1 {
        run_timer(&preset_1);
    } else if user_choice == 2 {
        run_timer(&preset_2);
    } else if user_choice == 3 {
        run_timer(&preset_3);
    } else if user_choice == 4 {
        loop {
            println!("make your own presets! (press q to quit)");
            let mut input = String::new();
            _ = io::stdin().read_line(&mut input);
            let input = input.trim();
            if input != "q" {
                let user_created_presets: UserPreset = build_user_preset();
                user_presets.push(user_created_presets);
                _ = write_to_file(&user_presets);
            } else {
                break;
            }
        }
    } else if user_choice == 5 {
        let create_user_presets: Vec<UserPreset> = read_presets_file().unwrap();
        println!("pick a preset!");
        let mut input = String::new();
        _ = io::stdin().read_line(&mut input);
        let input: i32 = input.trim().parse().expect("could not convert to int");
        for user_preset in create_user_presets {
            if input == user_preset.index {
                run_user_timer(&user_preset)
            }
        }
    }

    fn countdown_timer(duration: i32, sound_file: &str) {
        for i in (0..duration).rev() {
            thread::sleep(Duration::from_secs(1));
            let hours = (i / 3600) % 99;
            let minutes = (i / 60) % 60;
            let seconds = i % 60;
            print!("\r{}:{}:{}", hours, minutes, seconds);
            io::stdout().flush().expect("could not render");
        }
        playsound::play_sound(sound_file);
    }

    fn run_timer(preset: &TimerPreset) {
        countdown_timer(preset.work_time, "./mixkit-bell-notification-933.wav");
        thread::sleep(Duration::from_secs(3));
        println!("\nStarting your break");
        countdown_timer(preset.break_time, "./mixkit-bell-notification-933.wav");
        println!("break time over!");
    }
    fn run_user_timer(preset: &UserPreset) {
        countdown_timer(preset.work_time, "./mixkit-bell-notification-933.wav");
        thread::sleep(Duration::from_secs(3));
        println!("Starting your break");
        countdown_timer(preset.break_time, "./mixkit-bell-notification-933.wav");
        println!("break time over!");
    }
}
