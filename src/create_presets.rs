use serde::{Deserialize, Serialize};
use std::io;
#[derive(Debug, Serialize, Deserialize)]
pub struct UserPreset {
    pub preset_name: String,
    pub work_time: i32,
    pub break_time: i32,
    pub index: i32,
}
#[derive(Debug)]
pub struct TimerPreset {
    pub index: i32,
    pub name: String,
    pub work_time: i32,
    pub break_time: i32,
}

pub fn build_preset(index: i32, name: String, work_time: i32, break_time: i32) -> TimerPreset {
    TimerPreset {
        index,
        name,
        work_time,
        break_time,
    }
}
pub fn build_user_preset() -> UserPreset {
    let mut preset_name = String::new();
    println!("give your preset a name!");
    _ = io::stdin().read_line(&mut preset_name);
    let mut work_time = String::new();
    println!("How long would you like to work for? (enter time in seconds)");
    _ = io::stdin().read_line(&mut work_time);
    let work_time: i32 = work_time.trim().parse().expect("could not convert");
    let mut break_time = String::new();
    println!("How long would you like your break to be? (enter time in seconds)");
    _ = io::stdin().read_line(&mut break_time);
    let break_time: i32 = break_time.trim().parse().expect("could not convert");
    let mut index = String::new();
    println!("what is the number of this preset? default=1");
    _ = io::stdin().read_line(&mut index);
    let index: i32 = index.trim().parse().expect("could not convert");
    UserPreset {
        preset_name,
        work_time,
        break_time,
        index,
    }
}
